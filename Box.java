import java.util.*;

public class Box {
    private boolean open;
    private ArrayList<Thing> contents = new ArrayList<>();
    private int placePrise;
    private int capacite;

    public Box(){
        this.open = false;
        this.capacite = -1;
        this.placePrise = 0;
    }

    public Box(boolean open, int capacite){
        this.open = open;
        this.capacite = capacite;
        this.placePrise = 0;
    }

    public int getPlacePrise(){
        return this.placePrise;
    }


    public void add(Thing truc) {
        this.contents.add(truc);
        this.placePrise += truc.volume();
    }
    public List<Thing> getDansBoite() {
        return this.contents;
    }
    
    public boolean contains(String nom){
        Thing thing = new Thing(nom);
        return this.contents.contains(thing);
    }

    public void enleve(Thing truc) throws ExceptionExistePas {
        if (this.contents.contains(truc)){
            this.contents.remove(truc);
            this.placePrise -= truc.volume();
        }
        else{
            throw new ExceptionExistePas();
        }
    }

    public void open(){
        this.open = true;
    }

    public void close(){
        this.open = false;
    }

    public boolean isOpen(){
        return this.open;
    }

    public String actionLook() {
        if (this.isOpen()){
            String string = "la boite contient:";
            for (Thing thing:this.contents){
            string+=thing.getName()+", ";
            }
        return string; 
        }
        else{
            return "la boite est fermee";
        }
        
    }


    public void setCapacity(int num){
        this.capacite =num;
    }

    public int capacity(){
        return this.capacite;
    }

    public boolean hasRoomFor(Thing t){
        if (this.capacite == -1 || this.capacite-this.placePrise >= t.volume()){
            return true;
        }
        return false;
    }

    public Thing find(String nomObj) throws ExceptionExistePas{
        for (Thing obj : this.contents){
            if (obj.getName().equals(nomObj)){
                return obj;
            }
        }
        throw new ExceptionExistePas();
    }

    public void actionAdd(Thing t) throws ExceptionExistePas {
        if (t == null){throw new ExceptionExistePas();}
        if (this.capacite - this.placePrise > t.volume() && this.isOpen()){
            this.contents.add(t);
        }
        else{
            throw new ExceptionExistePas();
        }
    }
}
