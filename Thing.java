public class Thing{

    private String name;
    private int volume;

    public Thing(String nom){
        this.name = nom;
        this.volume =1;
    }

    public Thing(String nom, int vol){
        this.name = nom;
        this.volume = vol;
    }


    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Thing){
            Thing thing = (Thing) obj;
            return this.getName().equals(thing.getName());
        }
        return false;
    }

    public String getName() {
        return this.name;
    }

    public int volume(){return this.volume;}


    public void setName(String n){ this.name = n;}


    public boolean hasName(String n) {return this.name.equals(n);}


}