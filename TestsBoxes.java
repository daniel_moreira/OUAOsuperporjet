import org.junit.*;

/**
 * TestsBoxes
 */
public class TestsBoxes {
    @Test
    public void testBoxCreate() {
        Box b = new Box();
    }
    @Test
    public void testBoxAdd(){
    Box b = new Box();
    b.add(new Thing("truc1"));
    b.add(new Thing("truc2"));
    }
    
    @Test(expected = ArithmeticException.class)
    public void divisionWithException() {
    int i = 1/0;
    }
    
    //@Test
    //public void testBoxContains(){
    //    Box b = new Box();
    //    Thing truc5 = new Thing("truc1");
    //    b.add(truc5);
    //    Assert.assertTrue(b.contains(truc5));
    //}
    @Test
    public void testBoxEnleve() throws ExceptionExistePas {
        Box b = new Box();
        Box b2 = new Box();
        b2.add(new Thing("truc1"));
        b.add(new Thing("truc2"));
        Assert.assertNotEquals(b2.getDansBoite(),b.getDansBoite());
    }
    @Test
    public void testisOpen() {
        Box b = new Box();
        Thing truc1 = new Thing("truc1");
        b.add(truc1);
        b.add(new Thing("truc2"));
        Assert.assertFalse(b.isOpen());
        b.open();
        Assert.assertTrue(b.isOpen());
        b.close();
        Assert.assertFalse(b.isOpen());
    }
    @Test
    public void testActionLook(){
        Box b = new Box();
        Thing truc1 = new Thing("truc1");
        b.add(truc1);
        b.add(new Thing("truc2"));
        Assert.assertEquals(b.actionLook(), "la boite est fermee");
        b.open();
        Assert.assertNotEquals(b.actionLook(), "la boite est fermee");
    }
    @Test
    public void testCapacity() {
        Box b = new Box();
        b.setCapacity(5);
        Assert.assertEquals(b.capacity(),5);
    }
    @Test
    public void testHasRoomFor() {
        Box b = new Box();
        Thing truc1 = new Thing("truc1");
        Assert.assertTrue(b.hasRoomFor(truc1));
        b.setCapacity(0);
        Assert.assertFalse(b.hasRoomFor(truc1));
    }
    @Test
    public void testActionAdd() throws ExceptionExistePas{
        Box b= new Box();
        Thing truc2 = new Thing("null", 1);
        b.open();
        b.setCapacity(5);
        b.actionAdd(truc2);
        Assert.assertNotEquals(b.capacity(),b.getPlacePrise());
    }
    @Test (expected = ExceptionExistePas.class)
    public void testActionAddExeptionNull() throws ExceptionExistePas{
        Box b = new Box();
        b.open();
        b.setCapacity(5);
        b.actionAdd(null);
    }
    @Test (expected = ExceptionExistePas.class)
    public void testActionAddExeptionTropGrand() throws ExceptionExistePas{
        Box b = new Box();
        b.open();
        b.setCapacity(5);
        Thing truc1 = new Thing("null", 60);
        b.actionAdd(truc1);
    }

}